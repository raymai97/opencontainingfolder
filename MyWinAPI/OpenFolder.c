#include "OpenFolder.h"
#include "Pidl.h"
#include <ShlObj.h>
#include <shellapi.h>

#ifndef UNICODE
#define MY_A_W(x)	x "A"
#else
#define MY_A_W(x)	x "W"
#endif

#define MY_CHECK_HR_AND_ASSERT(x) \
	if (FAILED(hr)) goto eof; \
	if (!(x)) { hr = E_UNEXPECTED; goto eof; }

#define MY_COM_RELEASE(p) \
	if (p) { p->lpVtbl->Release(p); p = NULL; }

#define MY_COM_INIT() \
	hr = CoInitialize(NULL); \
	if (FAILED(hr)) goto eof; \
	coinit = TRUE;

#define MY_COM_UNINIT() \
	if (coinit) { CoUninitialize(); coinit = FALSE; }


static LPVOID MyAlloc(SIZE_T cb)
{
	LPVOID ptr = HeapAlloc(GetProcessHeap(), 0, cb);
	if (ptr) { ZeroMemory(ptr, cb); }
	return ptr;
}
static void MyFree(LPVOID ptr)
{
	HeapFree(GetProcessHeap(), 0, ptr);
}

/*
	Private Mini Helper .................
*/

static BOOL MyNoActiveDesktop(void)
{
	HRESULT hr = E_FAIL;
	IUnknown *pTest = NULL;
	hr = CoCreateInstance(&CLSID_ActiveDesktop, NULL, CLSCTX_ALL,
		&IID_IUnknown, (void**)&pTest);
	if (pTest) { MY_COM_RELEASE(pTest); }
	return FAILED(hr);
}

static void MyPidl_TrimLast(LPITEMIDLIST pidl)
{
	Pidl_LastIdMut(pidl)->mkid.cb = 0;
}


/*
	Overall Strategies Declaration ............
*/

static FARPROC MyGetFn_SHOpenFolderAndSelectItems(void)
{
	return GetProcAddress(LoadLibraryA("shell32"),
		"SHOpenFolderAndSelectItems");
}
static HRESULT My_SHOpenFolderAndSelectItems(
	LPCITEMIDLIST pidlItemOrParent, UINT itemCount,
	LPCITEMIDLIST *itemPidls, DWORD flags)
{
	typedef HRESULT(WINAPI *fn_t)(LPCITEMIDLIST, UINT, LPCITEMIDLIST*, DWORD);
	fn_t fn = (fn_t)MyGetFn_SHOpenFolderAndSelectItems();
	if (fn) {
		return fn(pidlItemOrParent, itemCount, itemPidls, flags);
	}
	return E_NOTIMPL;
}
static HRESULT MyOpenAndSelect_via_OfficialAPI(
	LPCITEMIDLIST pidlItem);


static FARPROC MyGetFn_SHGetIDispatchForFolder(void)
{
	if (MyNoActiveDesktop()) {
		return NULL; /* see NOTE-1 for reason */
	}
	return GetProcAddress(LoadLibraryA("shdocvw"),
		"SHGetIDispatchForFolder");
}
static HRESULT My_SHGetIDispatchForFolder(
	LPCITEMIDLIST pidl, IWebBrowserApp **pp)
{
	typedef HRESULT(WINAPI *fn_t)(LPCITEMIDLIST, IWebBrowserApp**);
	fn_t fn = (fn_t)MyGetFn_SHGetIDispatchForFolder();
	if (fn) {
		HRESULT hr = fn(pidl, pp);
		if (FAILED(hr)) {
			Sleep(100); /* see NOTE-1 for reason */
			hr = fn(pidl, pp);
		}
		return hr;
	}
	return E_NOTIMPL;
}
static HRESULT MyOpenFolder_via_SHGetIDispatch(
	LPCITEMIDLIST pidlFolder);
static HRESULT MyOpenAndSelect_via_SHGetIDispatch(
	LPCITEMIDLIST pidlItem);


static HRESULT My_ShellExecuteEx(SHELLEXECUTEINFO *pInfo)
{
	typedef BOOL(WINAPI *fn_t)(SHELLEXECUTEINFO*);
	fn_t fn = (fn_t)GetProcAddress(LoadLibraryA("shell32"),
		MY_A_W("ShellExecuteEx"));
	if (fn) {
		if (fn(pInfo)) { return S_OK; }
		return HRESULT_FROM_WIN32(GetLastError());
	}
	return E_NOTIMPL;
}
static HRESULT MyOpenFolder_via_ShellExecuteEx(
	LPCITEMIDLIST pidlFolder);


static HRESULT MyOpenAndSelect_via_ExplorerCmdLine(
	LPCTSTR szPath);


/* ------ Win95 Global PIDL function ------ */
static FARPROC MyGetFn_ILGlobalClone(void)
{
	return GetProcAddress(LoadLibraryA("shell32"), (LPCSTR)20);
}
static FARPROC MyGetFn_ILGlobalFree(void)
{
	return GetProcAddress(LoadLibraryA("shell32"), (LPCSTR)156);
}
static LPITEMIDLIST My_ILGlobalClone(LPCITEMIDLIST pidl)
{
	typedef LPITEMIDLIST(WINAPI *fn_t)(LPCITEMIDLIST);
	fn_t fn = (fn_t)MyGetFn_ILGlobalClone();
	if (fn) {
		return fn(pidl);
	}
	return NULL;
}
static BOOL My_ILGlobalFree(LPITEMIDLIST pidl)
{
	typedef BOOL(WINAPI *fn_t)(LPITEMIDLIST);
	fn_t fn = (fn_t)MyGetFn_ILGlobalFree();
	if (fn) {
		return fn(pidl);
	}
	return FALSE;
}
/* ------ NT4.0 Shell-Wrapped Process Memory function ------ */
static FARPROC MyGetFn_SHAllocShared(void)
{
	return GetProcAddress(LoadLibraryA("shell32"), (LPCSTR)520);
}
static FARPROC MyGetFn_SHFreeShared(void)
{
	return GetProcAddress(LoadLibraryA("shell32"), (LPCSTR)523);
}
static HANDLE My_SHAllocShared(LPCVOID pData, DWORD cbData, DWORD pid)
{
	typedef HANDLE(WINAPI *fn_t)(LPCVOID, DWORD, DWORD);
	fn_t fn = (fn_t)MyGetFn_SHAllocShared();
	if (fn) {
		return fn(pData, cbData, pid);
	}
	return NULL;
}
static BOOL My_SHFreeShared(HANDLE hData, DWORD pid)
{
	typedef BOOL(WINAPI *fn_t)(HANDLE, DWORD);
	fn_t fn = (fn_t)MyGetFn_SHFreeShared();
	if (fn) {
		return fn(hData, pid);
	}
	return FALSE;
}
/* ------ Other CabinetInternal helper function ------ */
static HWND My_GetShellWindow(void)
{
	typedef HWND(WINAPI *fn_t)(void);
	fn_t fn = (fn_t)GetProcAddress(LoadLibraryA("user32"),
		"GetShellWindow");
	if (fn) { return fn(); }
	return NULL;
}
static HRESULT MyOpenAndSelect_via_CabinetInternal(
	LPCITEMIDLIST pidlItem);


/*
	Open Folder via SHGetIDispatch ..............
	for Win98 and later (and Win95/NT4.0 with Active Desktop)
*/

static HRESULT MyOpenFolder_via_SHGetIDispatch(
	LPCITEMIDLIST pidlFolder)
{
	HRESULT hr = 0;
	IWebBrowserApp *pWBA = NULL;
	HWND hwndExplorer = NULL;
	hr = My_SHGetIDispatchForFolder(pidlFolder, &pWBA);
	MY_CHECK_HR_AND_ASSERT(pWBA);
	hr = pWBA->lpVtbl->get_HWND(pWBA, (LPARAM*)&hwndExplorer);
	MY_CHECK_HR_AND_ASSERT(hwndExplorer);
	ShowWindow(hwndExplorer, SW_SHOWDEFAULT);
	SetForegroundWindow(hwndExplorer);
eof:
	MY_COM_RELEASE(pWBA);
	return hr;
}

/*
	Open Folder via ShellExecuteEx .............
	for Win95 and later
*/

static HRESULT MyOpenFolder_via_ShellExecuteEx(
	LPCITEMIDLIST pidlFolder)
{
	HRESULT hr = 0;
	SHELLEXECUTEINFO sei = { sizeof(sei) };
	/*
		MSDN say to put SEE_MASK_FLAG_DDEWAIT (SEE_MASK_NOASYNC)
		if process might end right after calling the API.
	*/
	sei.fMask = SEE_MASK_IDLIST | SEE_MASK_FLAG_DDEWAIT;
	sei.lpIDList = (void*)pidlFolder;
	sei.nShow = SW_SHOWDEFAULT;
	hr = My_ShellExecuteEx(&sei);
	return hr;
}

/*
	Open and Select via MS Official API  ...............
	for WinME and later
*/

static HRESULT MyOpenAndSelect_via_OfficialAPI(
	LPCITEMIDLIST pidlItem)
{
	return My_SHOpenFolderAndSelectItems(pidlItem, 0, NULL, 0);
}

/*
	Open and Select via SHGetIDispatch .................
	for Win98/98SE/2000 (and Win95/NT4.0 with Active Desktop)
*/

static HRESULT MySFVD_SelectItem(
	IShellFolderViewDual *pSFVD, LPCITEMIDLIST pidl, int sfsiFlags)
{
	HRESULT hr = 0;
	UINT cbPidl = 0;
	LPITEMIDLIST pidlCopy = NULL;
	SAFEARRAY sa = { 0 };
	VARIANT vt = { 0 };
	if (!pSFVD || !pidl) {
		hr = E_INVALIDARG; goto eof;
	}
	cbPidl = Pidl_GetSize(pidl);
	pidlCopy = CoTaskMemAlloc(cbPidl);
	if (!pidlCopy) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	CopyMemory(pidlCopy, pidl, cbPidl);
	sa.cbElements = 1;
	sa.cDims = 1;
	sa.pvData = pidlCopy;
	sa.rgsabound[0].cElements = cbPidl;
	vt.vt = VT_ARRAY | VT_UI1;
	vt.parray = &sa;
	hr = pSFVD->lpVtbl->SelectItem(pSFVD, &vt, sfsiFlags);
eof:
	if (pidlCopy) { CoTaskMemFree(pidlCopy); }
	return hr;
}

static HRESULT MyOpenAndSelect_via_SHGetIDispatch(
	LPCITEMIDLIST pidlItem)
{
	HRESULT hr = 0;
	LPITEMIDLIST pidlParent = NULL;
	IWebBrowserApp *pWBA = NULL;
	IDispatch *pWBAD = NULL;
	IShellFolderViewDual *pSFVD = NULL;
	HWND hwndExplorer = NULL;
	if (!MyGetFn_SHGetIDispatchForFolder()) {
		hr = E_NOTIMPL; goto eof;
	}
	/* parent = item.Clone().TrimLast() */
	hr = Pidl_Clone(pidlItem, &pidlParent);
	MY_CHECK_HR_AND_ASSERT(pidlParent);
	MyPidl_TrimLast(pidlParent);
	/* Open parent folder */
	hr = My_SHGetIDispatchForFolder(pidlParent, &pWBA);
	MY_CHECK_HR_AND_ASSERT(pWBA);
	/* Get the opened Explorer window */
	hr = pWBA->lpVtbl->get_HWND(pWBA, (LPARAM*)&hwndExplorer);
	MY_CHECK_HR_AND_ASSERT(hwndExplorer);
	/* Query for SFVD (ShellFolderViewDual) to select item */
	hr = pWBA->lpVtbl->get_Document(pWBA, &pWBAD);
	MY_CHECK_HR_AND_ASSERT(pWBAD);
	hr = pWBAD->lpVtbl->QueryInterface(pWBAD,
		&IID_IShellFolderViewDual, (void**)&pSFVD);
	MY_CHECK_HR_AND_ASSERT(pSFVD);
	/* In older Windows, SelectItem() doesn't accept absolute PIDL. */
	hr = MySFVD_SelectItem(pSFVD, Pidl_LastId(pidlItem),
		SVSI_SELECT | SVSI_DESELECTOTHERS |
		SVSI_ENSUREVISIBLE | SVSI_FOCUSED);
	/* Activate the opened window */
	ShowWindow(hwndExplorer, SW_SHOWDEFAULT);
	SetForegroundWindow(hwndExplorer);
eof:
	MY_COM_RELEASE(pSFVD);
	MY_COM_RELEASE(pWBAD);
	MY_COM_RELEASE(pWBA);
	if (pidlParent) { Pidl_FreePidl(pidlParent); }
	return hr;
}

/*
	Open and Select Path with Command Line ...........
	for Win95/NT4.0 without Active Desktop
*/

static HRESULT MyOpenAndSelect_via_ExplorerCmdLine(
	LPCTSTR szPath)
{
	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	TCHAR szCmdLine[999] = { 0 }, *p = szCmdLine;
	GetWindowsDirectory(p, MAX_PATH);
	for (; *p; ++p);
	wsprintf(p, TEXT("\\explorer.exe /select,%s"), szPath);
	return CreateProcess(NULL, szCmdLine, NULL, NULL, FALSE, 0,
		NULL, NULL, &si, &pi) ? S_OK : E_FAIL;
}

/*
	Open and Select with Cabinet Internal .............
	for Win95/NT4.0 without Active Desktop

	Assume Explorer.exe is always single process.
*/

enum CabinetInternal {
	WFFO_ADD = 0x01,
	WFFO_REMOVE = 0x02,
	WFFO_WAIT = 0x04,
	WFFO_SIGNAL = 0x08,
	/* wParam = SVSI_xxx, lParam = pidl */
	CWM_SELECTITEM = (WM_USER + 5),
	/*
		if Win95, wParam = 0, lParam = global PIDL
		if NT4.0, wParam = PID passed to SHAllocShared
			lParam = SHAllocShared(MY_ComparePidl_t)
	*/
	CWM_COMPAREPIDL = (WM_USER + 9)
};

typedef struct MY_ComparePidl MY_ComparePidl_t;
struct MY_ComparePidl {
	BOOL fParent;
	LPITEMIDLIST pidl;
};

static HWND MyFindCabinetWindow(
	BOOL isNT4, DWORD hComparePidl_pidOwner,
	HANDLE hComparePidl,
	LPCITEMIDLIST global_pidlFolder)
{
	LPCTSTR const szClass = TEXT("CabinetWClass");
	HWND hwndCurr = NULL, hwndPrev = NULL;
	WPARAM wParam = 0;
	LPARAM lParam = 0;
	if (isNT4) {
		wParam = (WPARAM)hComparePidl_pidOwner;
		lParam = (LPARAM)hComparePidl;
	}
	else {
		lParam = (LPARAM)global_pidlFolder;
	}
	while (hwndCurr = FindWindowEx(NULL, hwndPrev, szClass, NULL))
	{
		if (SendMessage(hwndCurr, CWM_COMPAREPIDL, wParam, lParam))
		{
			return hwndCurr;
		}
		hwndPrev = hwndCurr;
	}
	return NULL;
}

static HRESULT MyOpenAndSelect_via_CabinetInternal(
	LPCITEMIDLIST pidlItem)
{
	UINT const findCab_sleepMs = 100;
	UINT const findCab_retryCount = 30;

	HRESULT hr = 0;
	LPITEMIDLIST pidlParent = NULL;
	UINT cbPidlParent = 0;
	UINT cbPidlItem = 0, i = 0;

	BOOL isNT4 = FALSE;
	HWND hwndShell = 0;
	DWORD pidShell = 0;
	MY_ComparePidl_t *pComparePidl = NULL;
	HANDLE shAlloc_comparePidl = NULL;
	HANDLE shAlloc_pidlItem = NULL;
	LPITEMIDLIST global_pidlParent = NULL;
	LPITEMIDLIST global_pidlItem = NULL;

	HWND hwndCab = NULL;
	WPARAM wParam = 0;
	LPARAM lParam = 0;
	LRESULT lResult = 0;
	
	/*
		pidlParent = pidlItem.Clone().TrimLast()
		pidlItem = pidlItem.LastId()
		and get cbPidlParent, cbPidlItem...
	*/
	hr = Pidl_Clone(pidlItem, &pidlParent);
	MY_CHECK_HR_AND_ASSERT(pidlParent);
	MyPidl_TrimLast(pidlParent);
	cbPidlParent = Pidl_GetSize(pidlParent);
	pidlItem = Pidl_LastId(pidlItem);
	cbPidlItem = Pidl_GetSize(pidlItem);
	/*
		Get Shell window and its PID.
		If NULL, then Explorer not running, halt.
	*/
	hwndShell = My_GetShellWindow();
	GetWindowThreadProcessId(hwndShell, &pidShell);
	if (!pidShell) {
		hr = E_UNEXPECTED; goto eof;
	}
	/*
		For NT4.0, CopyAlloc the PIDLs into the Shell process memory.
	*/
	if (isNT4 = (MyGetFn_SHAllocShared() != NULL))
	{
		UINT cbComparePidl = sizeof(BOOL) + cbPidlParent;
		pComparePidl = MyAlloc(cbComparePidl);
		if (!pComparePidl) {
			hr = E_OUTOFMEMORY; goto eof;
		}
		pComparePidl->fParent = FALSE;
		CopyMemory(&(pComparePidl->pidl), pidlParent, cbPidlParent);
		shAlloc_comparePidl = My_SHAllocShared(
			pComparePidl, cbComparePidl, pidShell);
		if (!shAlloc_comparePidl) {
			hr = E_OUTOFMEMORY; goto eof;
		}
		shAlloc_pidlItem = My_SHAllocShared(
			pidlItem, cbPidlItem, pidShell);
		if (!shAlloc_pidlItem) {
			hr = E_OUTOFMEMORY; goto eof;
		}
	}
	else /* For Win95, use Global PIDL instead. */
	{ 
		global_pidlParent = My_ILGlobalClone(pidlParent);
		if (!global_pidlParent) {
			hr = E_OUTOFMEMORY; goto eof;
		}
		global_pidlItem = My_ILGlobalClone(pidlItem);
		if (!global_pidlItem) {
			hr = E_OUTOFMEMORY; goto eof;
		}
	}
	/*
		Open the pidlParent folder.
	*/
	hr = MyOpenFolder_via_ShellExecuteEx(pidlParent);
	if (FAILED(hr)) goto eof;
	/*
		Find cabinet window that currently browsing the pidlParent.
	*/
	for (i = 0; i < findCab_retryCount; ++i)
	{
		hwndCab = MyFindCabinetWindow(isNT4, pidShell,
			shAlloc_comparePidl, global_pidlParent);
		if (hwndCab) { break; }
		Sleep(findCab_sleepMs);
	}
	if (!hwndCab) {
		hr = HRESULT_FROM_WIN32(ERROR_TIMEOUT); goto eof;
	}
	/*
		Command that window to select desired item.
	*/
	wParam = SVSI_SELECT | SVSI_DESELECTOTHERS |
		SVSI_ENSUREVISIBLE | SVSI_FOCUSED;
	if (isNT4) {
		lParam = (LPARAM)shAlloc_pidlItem;
	}
	else {
		lParam = (LPARAM)global_pidlItem;
	}
	lResult = SendMessage(hwndCab, CWM_SELECTITEM, wParam, lParam);
	/*
		Not sure why but lResult always return 0, even if succeeded.
		Let's ignore it for now.
	*/
eof:
	if (global_pidlItem) {
		My_ILGlobalFree(global_pidlItem);
	}
	if (global_pidlParent) {
		My_ILGlobalFree(global_pidlParent);
	}
	if (shAlloc_pidlItem) {
		My_SHFreeShared(shAlloc_pidlItem, pidShell);
	}
	if (shAlloc_comparePidl) {
		My_SHFreeShared(shAlloc_comparePidl, pidShell);
	}
	if (pComparePidl) {
		MyFree(pComparePidl);
	}
	if (pidlParent) {
		Pidl_FreePidl(pidlParent);
	}
	return hr;
}


/*
	Exported Functions .................
*/

EXTERN_C
HRESULT __stdcall
OpenFolderAndSelectItemByPath(
	LPCTSTR szPath)
{
	HRESULT hr = 0;
	BOOL coinit = FALSE;
	LPITEMIDLIST pidl = NULL;
	if (!szPath) {
		hr = E_INVALIDARG; goto eof;
	}
	if (GetFileAttributes(szPath) == -1) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	/*
		Since Win7, the ExplorerCmdLine method will cause Explorer
		to spawn zombie process. So, use official API if possible.
	*/
	if (MyGetFn_SHOpenFolderAndSelectItems())
	{
		MY_COM_INIT();
		hr = Pidl_CreateFromPath(szPath, &pidl);
		MY_CHECK_HR_AND_ASSERT(pidl);
		hr = MyOpenAndSelect_via_OfficialAPI(pidl);
	}
	else /* It is safe to use this method. */
	{
		hr = MyOpenAndSelect_via_ExplorerCmdLine(szPath);
	}
eof:
	if (pidl) { Pidl_FreePidl(pidl); }
	MY_COM_UNINIT();
	return hr;
}

EXTERN_C
HRESULT __stdcall
OpenFolderAndSelectItemByPidl(
	LPCITEMIDLIST pidl)
{
	HRESULT hr = 0;
	BOOL coinit = FALSE;
	if (!pidl) {
		hr = E_INVALIDARG; goto eof;
	}
	MY_COM_INIT();
	hr = MyOpenAndSelect_via_OfficialAPI(pidl);
	if (hr == E_NOTIMPL) {
		hr = MyOpenAndSelect_via_SHGetIDispatch(pidl);
	}
	if (hr == E_NOTIMPL) {
		hr = MyOpenAndSelect_via_CabinetInternal(pidl);
	}
eof:
	MY_COM_UNINIT();
	return hr;
}

EXTERN_C
HRESULT __stdcall
OpenFolderByPath(
	LPCTSTR szPath)
{
	HRESULT hr = 0;
	BOOL coinit = FALSE;
	LPITEMIDLIST pidl = NULL;
	if (!szPath) {
		hr = E_INVALIDARG; goto eof;
	}
	if (GetFileAttributes(szPath) == -1) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	MY_COM_INIT();
	hr = Pidl_CreateFromPath(szPath, &pidl);
	MY_CHECK_HR_AND_ASSERT(pidl);
	hr = MyOpenFolder_via_SHGetIDispatch(pidl);
	if (hr == E_NOTIMPL) {
		hr = MyOpenFolder_via_ShellExecuteEx(pidl);
	}
eof:
	if (pidl) { Pidl_FreePidl(pidl); }
	MY_COM_UNINIT();
	return hr;
}

EXTERN_C
HRESULT __stdcall
OpenFolderByPidl(
	LPCITEMIDLIST pidl)
{
	HRESULT hr = 0;
	BOOL coinit = FALSE;
	if (!pidl) {
		hr = E_INVALIDARG; goto eof;
	}
	MY_COM_INIT();
	hr = MyOpenFolder_via_SHGetIDispatch(pidl);
	if (hr == E_NOTIMPL) {
		hr = MyOpenFolder_via_ShellExecuteEx(pidl);
	}
eof:
	MY_COM_UNINIT();
	return hr;
}


/*
	NOTE-1: SHGetIDispatchForFolder() is problematic on Win95/NT4.0.
	Note that no such API pre-IE4. It is added after IE4 is installed.

	CASE IE4 + Active Desktop:
	If the folder window is not created upon calling the API,
	the API will create the folder window, but return bad HRESULT.
	If the folder window is already created,
	the API will reuse the window, return good HRESULT and pWBA.

	Workaround: If first call failed, wait a while and call again.

	CASE IE4 - Active Desktop:
	The API will create/reuse the folder window,
	but ALWAYS return E_ABORT.

	Workaround: Don't use this API if no Active Desktop.

	--- FUN FACT ---
	Installing Active Desktop will break the explorer '/select'
	command-line switch of the Explorer. The containing folder
	will still be opened, but the item will not be selected.
	Fortunately only NT4.0 has this issue.
*/
