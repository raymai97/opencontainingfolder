#include "Pidl.h"
#include "Pidl_EXTEND.h"
#include <ShlObj.h>

#ifndef UNICODE
#define MY_A_W(x)	x "A"
#else
#define MY_A_W(x)	x "W"
#endif

#define MY_CHECK_HR_AND_ASSERT(x) \
	if (FAILED(hr)) goto eof; \
	if (!(x)) { hr = E_UNEXPECTED; goto eof; }

#define MY_COM_RELEASE(p) \
	if (p) { p->lpVtbl->Release(p); p = NULL; }

#define MY_COM_INIT() \
	hr = CoInitialize(NULL); \
	if (FAILED(hr)) goto eof; \
	coinit = TRUE;

#define MY_COM_UNINIT() \
	if (coinit) { CoUninitialize(); coinit = FALSE; }


static LPVOID MyAlloc(SIZE_T cb)
{
	return CoTaskMemAlloc(cb);
}
static void MyFree(LPVOID ptr)
{
	CoTaskMemFree(ptr);
}

/* Wide <==> Multi ..... */

static LPTSTR MyCopyAlloc_TFromAnsi(LPCSTR szSrc)
{
	LPTSTR psz = NULL;
#ifdef UNICODE
	int cb = MultiByteToWideChar(CP_ACP, 0, szSrc, -1, NULL, 0);
	if (psz = MyAlloc(cb)) {
		 MultiByteToWideChar(CP_ACP, 0, szSrc, -1, psz, cb);
	}
#else
	int cb = lstrlenA(szSrc) + 1;
	if (psz = MyAlloc(cb)) {
		lstrcpynA(psz, szSrc, cb);
	}
#endif
	return psz;
}

static LPTSTR MyCopyAlloc_TFromWide(LPCWSTR szSrc)
{
	LPTSTR psz = NULL;
	int cch = 0, cb = 0;
#ifdef UNICODE
	cch = lstrlenW(szSrc) + 1;
	cb = sizeof(WCHAR) * cch;
	if (psz = MyAlloc(cb)) {
		lstrcpynW(psz, szSrc, cch);
	}
#else /* no UNICODE */
	cb = WideCharToMultiByte(CP_ACP, 0, szSrc, -1,
		NULL, 0, NULL, NULL);
	if (psz = MyAlloc(cb)) {
		WideCharToMultiByte(CP_ACP, 0, szSrc, -1,
			psz, cb, NULL, NULL);
	}
#endif
	return psz;
}

static LPWSTR MyCopyAlloc_WideFrom(LPCTSTR szSrc)
{
	LPWSTR psz = NULL;
	int cch = 0, cb = 0;
#ifdef UNICODE
	cch = lstrlenW(szSrc) + 1;
	cb = sizeof(WCHAR) * cch;
	if (psz = MyAlloc(cb)) {
		lstrcpynW(psz, szSrc, cch);
	}
#else /* no UNICODE */
	cb = MultiByteToWideChar(CP_ACP, 0, szSrc, -1, NULL, 0);
	if (psz = MyAlloc(cb)) {
		 MultiByteToWideChar(CP_ACP, 0, szSrc, -1, psz, cb);
	}
#endif
	return psz;
}

/* ..... Wide <==> Multi */

/*
	IShellLink method is good enough for most situation.
	However, in modern Windows like Win10, due to some stupid decision
	made by Microsoft, passing certain special folder path, such as
	User Documents to IShellLink::GetIDList(), would give you a special
	PIDL that when you use it to SHOpenFolderAnd...(), it would not
	locate the "real" folder, but the fake one in "This PC".
	PIDL made with ILCreateFromPath() is immuned from this crap design.
*/
static LPITEMIDLIST MyILCreateFromPath(LPCTSTR szPath)
{
	typedef LPITEMIDLIST(WINAPI *fn_t)(LPCTSTR);
	fn_t fn = (fn_t)GetProcAddress(LoadLibraryA("shell32"),
		MY_A_W("ILCreateFromPath"));
	if (fn) {
		return fn(szPath);
	}
	return NULL;
}

static HRESULT MyCreateFromPath(
	LPCTSTR szPath, LPITEMIDLIST *ppidl)
{
	HRESULT hr = 0;
	BOOL coinit = FALSE;
	IShellLink *psl = NULL;
	LPITEMIDLIST pidl = NULL;
	MY_COM_INIT();
	if (pidl = MyILCreateFromPath(szPath))
	{
		*ppidl = pidl; goto eof;
	}
	hr = CoCreateInstance(&CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
		&IID_IShellLink, (void**)&psl);
	MY_CHECK_HR_AND_ASSERT(psl);
	hr = psl->lpVtbl->SetPath(psl, szPath);
	if (FAILED(hr)) goto eof;
	hr = psl->lpVtbl->GetIDList(psl, &pidl);
	MY_CHECK_HR_AND_ASSERT(pidl);
	*ppidl = pidl;
eof:
	MY_COM_RELEASE(psl);
	MY_COM_UNINIT();
	return hr;
}

static HRESULT MyCreateFromUnk(
	IUnknown *pUnk, LPITEMIDLIST *ppidl)
{
	HRESULT hr = 0;
	MY_IPersistIDList_t *pIDL = NULL;
	MY_IPersistFolder2_t *pF2 = NULL;
	hr = pUnk->lpVtbl->QueryInterface(pUnk, &s_IID_IPersistIDList, (void**)&pIDL);
	if (SUCCEEDED(hr))
	{
		if (!pIDL) { hr = E_UNEXPECTED; goto eof; }
		hr = pIDL->lpVtbl->GetIDList(pIDL, ppidl);
		goto eof;
	}
	hr = pUnk->lpVtbl->QueryInterface(pUnk, &s_IID_IPersistFolder2, (void**)&pF2);
	if (SUCCEEDED(hr))
	{
		if (!pF2) { hr = E_UNEXPECTED; goto eof; }
		hr = pF2->lpVtbl->GetCurFolder(pF2, ppidl);
		goto eof;
	}
eof:
	MY_COM_RELEASE(pF2);
	MY_COM_RELEASE(pIDL);
	return hr;
}

/* for MyGetNameAlloc() */
static HRESULT MyParseAndFreeStrret(
	STRRET *psr, LPTSTR *ppsz)
{
	LPTSTR psz = NULL;
	if (!psr || !ppsz) { return E_INVALIDARG; }
	if (psr->uType == STRRET_WSTR)
	{
		if (!(psr->pOleStr)) { return E_INVALIDARG; }
		psz = MyCopyAlloc_TFromWide(psr->pOleStr);
		if (!psz) { return E_OUTOFMEMORY; }
		CoTaskMemFree(psr->pOleStr);
		psr->pOleStr = NULL;
	}
	else if (psr->uType == STRRET_CSTR)
	{
		psz = MyCopyAlloc_TFromAnsi(psr->cStr);
		if (!psz) { return E_OUTOFMEMORY; }
	}
	else {
		return E_INVALIDARG;
	}
	*ppsz = psz;
	return S_OK;
}

static HRESULT MyGetNameAlloc(
	LPCITEMIDLIST pidl, DWORD shgdn, LPTSTR *ppsz)
{
	HRESULT hr = 0;
	BOOL coinit = FALSE;
	IShellFolder *psf = NULL;
	STRRET sr = { 0 };
	LPTSTR psz = NULL;
	MY_COM_INIT();
	hr = CoCreateInstance(&CLSID_ShellDesktop, NULL, CLSCTX_INPROC_SERVER,
		&IID_IShellFolder, (void**)&psf);
	MY_CHECK_HR_AND_ASSERT(psf);
	hr = psf->lpVtbl->GetDisplayNameOf(psf, pidl, shgdn, &sr);
	if (FAILED(hr)) goto eof;
	hr = MyParseAndFreeStrret(&sr, &psz);
	if (FAILED(hr)) goto eof;
	*ppsz = psz;
eof:
	MY_COM_RELEASE(psf);
	MY_COM_UNINIT();
	return hr;
}

/* for MyGetPathAlloc() */
static HRESULT MyShGetPathFromIDList(
	LPCITEMIDLIST pidl, LPTSTR buf)
{
	typedef BOOL(WINAPI *fn_t)(LPCITEMIDLIST, LPTSTR);
	fn_t fn = (fn_t)GetProcAddress(LoadLibraryA("shell32"),
		MY_A_W("SHGetPathFromIDList"));
	if (fn) {
		return fn(pidl, buf) ? S_OK : E_FAIL;
	}
	return E_NOTIMPL;
}

static HRESULT MyToShellItem(
	LPCITEMIDLIST pidl, IShellItem **ppsi)
{
	typedef HRESULT(WINAPI *fn_t)(LPCITEMIDLIST,
		IShellItem*, LPCITEMIDLIST, IShellItem**);
	fn_t fn = (fn_t)GetProcAddress(LoadLibraryA("shell32"),
		"SHCreateShellItem");
	if (fn) {
		return fn(NULL, NULL, pidl, ppsi);
	}
	return E_NOTIMPL;
}

static HRESULT MyGetPathAlloc(
	LPCITEMIDLIST pidl, LPTSTR *ppsz)
{
	HRESULT hr = 0;
	LPTSTR psz = NULL;
	psz = MyAlloc(sizeof(TCHAR) * MAX_PATH);
	if (!psz) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	hr = MyShGetPathFromIDList(pidl, psz);
	if (FAILED(hr)) goto eof;
	*ppsz = psz;
eof:
	if (FAILED(hr) && psz) { MyFree(psz); }
	return hr;
}

EXTERN_C
void __stdcall
Pidl_FreePidl(
	LPITEMIDLIST pidl)
{
	/* pidl not me-alloc! Don't change to MyFree()! */
	CoTaskMemFree(pidl);
}

EXTERN_C
void __stdcall
Pidl_FreeStr(
	LPTSTR psz)
{
	MyFree(psz);
}

EXTERN_C
HRESULT __stdcall
Pidl_CreateFromPath(
	LPCTSTR szPath,
	LPITEMIDLIST *ppidl)
{
	if (szPath && ppidl) {
		return MyCreateFromPath(szPath, ppidl);
	}
	return E_INVALIDARG;
}

EXTERN_C
HRESULT __stdcall
Pidl_CreateFromUnk(
	IUnknown *pUnk,
	LPITEMIDLIST *ppidl)
{
	if (pUnk && ppidl) {
		return MyCreateFromUnk(pUnk, ppidl);
	}
	return E_INVALIDARG;
}

EXTERN_C
HRESULT __stdcall
Pidl_GetDispNameAlloc(
	LPCITEMIDLIST pidl,
	LPTSTR *ppsz)
{
	if (pidl && ppsz) {
		return MyGetNameAlloc(pidl, SHGDN_NORMAL, ppsz);
	}
	return E_INVALIDARG;
}

EXTERN_C
HRESULT __stdcall
Pidl_GetParseNameAlloc(
	LPCITEMIDLIST pidl,
	LPTSTR *ppsz)
{
	if (pidl && ppsz) {
		return MyGetNameAlloc(pidl, SHGDN_NORMAL | SHGDN_FORPARSING, ppsz);
	}
	return E_INVALIDARG;
}

EXTERN_C
HRESULT __stdcall
Pidl_GetPathAlloc(
	LPCITEMIDLIST pidl,
	LPTSTR *ppsz)
{
	if (pidl && ppsz) {
		return MyGetPathAlloc(pidl, ppsz);
	}
	return E_INVALIDARG;
}

EXTERN_C
HRESULT __stdcall
Pidl_ToShellItem(
	LPCITEMIDLIST pidl,
	IShellItem **ppsi)
{
	if (pidl && ppsi) {
		return MyToShellItem(pidl, ppsi);
	}
	return E_INVALIDARG;
}

EXTERN_C
HRESULT __stdcall
Pidl_Clone(
	LPCITEMIDLIST pidl,
	LPITEMIDLIST *ppidl)
{
	HRESULT hr = 0;
	LPITEMIDLIST pidlNew = NULL;
	UINT cb = 0;
	if (!pidl || !ppidl) { hr = E_INVALIDARG; goto eof; }
	cb = Pidl_GetSize(pidl);
	/* pidl not me-alloc! Don't change to MyAlloc()! */
	pidlNew = CoTaskMemAlloc(cb);
	if (!pidlNew) { hr = E_OUTOFMEMORY; goto eof; }
	CopyMemory(pidlNew, pidl, cb);
	*ppidl = pidlNew;
eof:
	return hr;
}

EXTERN_C
UINT __stdcall
Pidl_GetSize(
	LPCITEMIDLIST pidl)
{
	UINT cbTotal = sizeof(USHORT);
	USHORT cb = 0;
	while (pidl && (cb = pidl->mkid.cb))
	{
		cbTotal += cb;
		pidl = (LPCITEMIDLIST)((BYTE*)pidl + cb);
	}
	return cbTotal;
}

EXTERN_C
LPCITEMIDLIST __stdcall
Pidl_LastId(
	LPCITEMIDLIST pidl)
{
	LPCITEMIDLIST pLast = pidl;
	USHORT cb = 0;
	while (pidl && (cb = pidl->mkid.cb))
	{
		pLast = pidl;
		pidl = (LPCITEMIDLIST)((BYTE*)pidl + cb);
	}
	return pLast;
}

EXTERN_C
LPITEMIDLIST __stdcall
Pidl_LastIdMut(
	LPITEMIDLIST pidl)
{
	return (LPITEMIDLIST)Pidl_LastId(pidl);
}

EXTERN_C
BOOL __stdcall
Pidl_IsEqual(
	LPCITEMIDLIST pidl1,
	LPCITEMIDLIST pidl2)
{
	USHORT cb1 = 0, cb2 = 0, i = 0;
	while ((cb1 = pidl1->mkid.cb) && (cb2 = pidl2->mkid.cb))
	{
		if (cb1 != cb2) {
			return FALSE;
		}
		for (i = 0; i < cb1; ++i) {
			if (pidl1->mkid.abID[i] != pidl2->mkid.abID[i]) {
				return FALSE;
			}
		}
		pidl1 = (LPCITEMIDLIST)((BYTE*)pidl1 + cb1);
		pidl2 = (LPCITEMIDLIST)((BYTE*)pidl2 + cb2);
	}
	return TRUE;
}
