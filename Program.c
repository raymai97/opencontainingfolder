#include <Windows.h>
#include <MyWinAPI/CmdlineArgv.h>
#include <MyWinAPI/OpenFolder.h>

static LPVOID MyAllocZero(SIZE_T cb)
{
	LPVOID ptr = HeapAlloc(GetProcessHeap(), 0, cb);
	if (ptr) { ZeroMemory(ptr, cb); }
	return ptr;
}
static void MyFree(LPVOID ptr)
{
	HeapFree(GetProcessHeap(), 0, ptr);
}

static LPTSTR MyCopyAllocRange(
	TCHAR const *p1, TCHAR const *p2)
{
	LPTSTR psz = NULL;
	SIZE_T cbCopy = (p2 - p1 + 1) * sizeof(TCHAR);
	SIZE_T cbAlloc = cbCopy + sizeof(TCHAR);
	psz = MyAllocZero(cbAlloc);
	if (psz) { CopyMemory(psz, p1, cbCopy); }
	return psz;
}

static LPTSTR MyCopyAllocDequote(
	TCHAR const *pStart)
{
	LPTSTR psz = NULL;
	TCHAR const *pEnd = NULL;
	if (pStart[0] == '\"') { ++pStart; }
	for (pEnd = pStart; *pEnd; ++pEnd)
	{
		if (pEnd[1] == '\"') {
			break;
		}
	}
	/*
		"C:\Windows"
		 ^        ^
		 pStart   pEnd
	*/
	psz = MyCopyAllocRange(pStart, pEnd);
	return psz;
}

/* Relative -> Absolute Path */
static HRESULT MyGetFullPathAlloc(LPCTSTR pszRelative, LPTSTR *ppszPath)
{
	HRESULT hr = 0;
	DWORD cchPath = 0;
	LPTSTR pszPath = NULL;
	cchPath = GetFullPathName(pszRelative, 0, NULL, NULL);
	if (!cchPath) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	pszPath = MyAllocZero(sizeof(TCHAR) * cchPath);
	if (!pszPath) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	cchPath = GetFullPathName(pszRelative, cchPath, pszPath, NULL);
	if (!cchPath) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	if (GetFileAttributes(pszPath) == -1) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	*ppszPath = pszPath;
eof:
	if (FAILED(hr)) {
		if (pszPath) { MyFree(pszPath); }
	}
	return hr;
}

/* cmd.exe -> C:\Windows\System32\cmd.exe */
static HRESULT MySearchPathAlloc(LPCTSTR pszName, LPTSTR *ppszPath)
{
	HRESULT hr = 0;
	DWORD cchPath = 0;
	LPTSTR pszPath = NULL;
	cchPath = SearchPath(NULL, pszName, NULL, 0, NULL, NULL);
	if (!cchPath) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	pszPath = MyAllocZero(sizeof(TCHAR) * cchPath);
	if (!pszPath) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	cchPath = SearchPath(NULL, pszName, NULL, cchPath, pszPath, NULL);
	if (!cchPath) {
		hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
	}
	*ppszPath = pszPath;
eof:
	if (FAILED(hr)) {
		if (pszPath) { MyFree(pszPath); }
	}
	return hr;
}

/* Search SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths */
static HRESULT MySearchAppPathIn(
	HKEY hkeyBase, LPCTSTR pszMidReg,
	LPCTSTR pszName, LPTSTR *ppszPath)
{
	HRESULT hr = 0;
	LONG err = 0;
	TCHAR szKey[999] = { 0 };
	HKEY hkey = NULL;
	DWORD dwType = 0;
	DWORD cbVal = 0;
	LPTSTR pszVal = NULL;
	LPTSTR pszPath = NULL;
	if (!pszMidReg) {
		pszMidReg = TEXT("");
	}
	/* Open the Reg Key */
	wsprintf(szKey,
		TEXT("SOFTWARE\\%sMicrosoft\\Windows\\")
		TEXT("CurrentVersion\\App Paths\\%s"),
		pszMidReg, pszName);
	err = RegOpenKeyEx(hkeyBase, szKey, 0, KEY_READ, &hkey);
	if (!hkey) {
		hr = HRESULT_FROM_WIN32(err); goto eof;
	}
	/* Query Reg Value of {Default} */
	err = RegQueryValueEx(hkey, NULL, NULL, &dwType, NULL, &cbVal);
	if (!cbVal) {
		hr = HRESULT_FROM_WIN32(err); goto eof;
	}
	pszVal = MyAllocZero(cbVal + sizeof(TCHAR));
	if (!pszVal) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	err = RegQueryValueEx(hkey, NULL, NULL, &dwType, (LPBYTE)pszVal, &cbVal);
	if (err) {
		hr = HRESULT_FROM_WIN32(err); goto eof;
	}
	pszPath = MyCopyAllocDequote(pszVal);
	if (!pszPath) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	/* Quick Check Reg Value */
	if (dwType == REG_SZ) { /* Use value as-is */ }
	else if (dwType == REG_EXPAND_SZ) /* Expand %var% */
	{
		DWORD cchExpand = 0;
		LPTSTR pszExpand = NULL;
		cchExpand = ExpandEnvironmentStrings(pszPath, NULL, 0);
		if (!cchExpand) {
			hr = HRESULT_FROM_WIN32(GetLastError()); goto eof;
		}
		pszExpand = MyAllocZero(cchExpand * sizeof(TCHAR));
		if (!pszExpand) {
			hr = E_OUTOFMEMORY; goto eof;
		}
		ExpandEnvironmentStrings(pszPath, pszExpand, cchExpand);
		MyFree(pszPath);
		pszPath = pszExpand;
	}
	else { hr = E_UNEXPECTED; goto eof; }
	*ppszPath = pszPath;
eof:
	if (FAILED(hr)) {
		if (pszPath) { MyFree(pszPath); }
	}
	if (pszVal) { MyFree(pszVal); }
	if (hkey) { RegCloseKey(hkey); }
	return hr;
}

/* Search App Paths in HKCU, HKLM, HKLM WOW64 */
static HRESULT MySearchAppPathAlloc(
	LPCTSTR pszName, LPTSTR *ppszPath)
{
	HRESULT hr = 0;
	hr = MySearchAppPathIn(
		HKEY_CURRENT_USER, NULL, pszName, ppszPath
		);
	if (SUCCEEDED(hr)) goto eof;
	hr = MySearchAppPathIn(
		HKEY_LOCAL_MACHINE, NULL, pszName, ppszPath
		);
	if (SUCCEEDED(hr)) goto eof;
	hr = MySearchAppPathIn(
		HKEY_LOCAL_MACHINE, TEXT("Wow6432Node\\"), pszName, ppszPath
		);
eof:
	return hr;
}

static LPTSTR MyErrMsgAlloc(HRESULT hr)
{
	LPTSTR pszMsg = NULL;
	DWORD cbMsg = 0;
	DWORD cchSysErr = 0;
	LPTSTR pszSysErr = NULL;
	cchSysErr = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS |
		FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&pszSysErr, 0, NULL);
	cbMsg = (cchSysErr + 99) * sizeof(TCHAR);
	pszMsg = MyAllocZero(cbMsg);
	if (!pszMsg) goto eof;
	wsprintf(pszMsg, TEXT("%s\n\nHRESULT 0x%.8X"), pszSysErr, hr);
eof:
	if (pszSysErr) { LocalFree(pszSysErr); }
	return pszMsg;
}

static BOOL MyDisableWow64FsRedirect(void)
{
	typedef BOOL(WINAPI*fn_t)(void **ppOld);
	fn_t fn = (fn_t)GetProcAddress(LoadLibraryA("kernel32"),
		"Wow64DisableWow64FsRedirection");
	if (fn) {
		void *pOld = NULL;
		return fn(&pOld);
	}
	return FALSE;
}

void APIENTRY RawMain(void)
{
	HRESULT hr = 0;
	TCHAR szMyExePath[MAX_PATH] = {0};
	TCHAR const *pszExeName = NULL;
	TCHAR const	*pszCmdline = NULL;
	LPCTSTR pszTarget = NULL;
	LPTSTR pszPath = NULL;
	LPTSTR pszPath2 = NULL;
	LPTSTR pszPath3 = NULL;
	LPTSTR pszPath4 = NULL;

	MyDisableWow64FsRedirect();
	/* Get current exe full path. */
	if (GetModuleFileName(NULL, szMyExePath, MAX_PATH))
	{
		TCHAR *p = NULL, *q = NULL;
		for (p = szMyExePath; *p; ++p);
		for (q = p; *q != '\\'; --q); ++q;
		pszExeName = q;
	}
	/* Get command line after argv[0]. */
	if (!CmdlineArgv_LocateArgByIndex(GetCommandLine(), 1, &pszCmdline, NULL))
	{
		TCHAR szText[999] = {0};
		wsprintf(szText,
			TEXT("Open Containing Folder by raymai97\n\n")
			TEXT("Usage: %s [path]\n\n")
			TEXT("where [path] can be\n")
			TEXT("- Absolute Path, like C:\\Windows \n")
			TEXT("- Relative Path, like \\ \n")
			TEXT("- EXE in system PATH, like cmd.exe \n")
			TEXT("- EXE in App Paths, like winword.exe \n"),
			pszExeName);
		MessageBox(NULL, szText, pszExeName, MB_ICONINFORMATION);
		goto eof;
	}
	/* CopyAlloc the command line. Remove quote if any. */
	pszPath = MyCopyAllocDequote(pszCmdline);
	if (!pszPath) {
		hr = E_OUTOFMEMORY;
		goto eof;
	}
	/* Perhaps Absolute Path? */
	if (pszPath[0] && pszPath[1] == ':') {
		pszTarget = pszPath;
		goto eof;
	}
	/* Perhaps Relative Path? */
	hr = MyGetFullPathAlloc(pszPath, &pszPath2);
	if (SUCCEEDED(hr)) {
		pszTarget = pszPath2;
		goto eof;
	}
	/* Search in system PATH. */
	hr = MySearchPathAlloc(pszPath, &pszPath3);
	if (SUCCEEDED(hr)) {
		pszTarget = pszPath3;
		goto eof;
	}
	/* Search in App Path registry. */
	hr = MySearchAppPathAlloc(pszPath, &pszPath4);
	if (SUCCEEDED(hr)) {
		pszTarget = pszPath4;
		goto eof;
	}
eof:
	if (pszTarget) {
		hr = OpenFolderAndSelectItemByPath(pszTarget);
	}
	if (FAILED(hr)) {
		LPTSTR pszErr = MyErrMsgAlloc(hr);
		MessageBox(NULL, pszErr ? pszErr :
			TEXT("FATAL ERROR"),
			pszExeName, MB_ICONERROR);
		if (pszErr) { MyFree(pszErr); }
	}
	if (pszPath4) { MyFree(pszPath4); }
	if (pszPath3) { MyFree(pszPath3); }
	if (pszPath2) { MyFree(pszPath2); }
	if (pszPath) { MyFree(pszPath); }
	ExitProcess(FAILED(hr) ? 1 : 0);
}
