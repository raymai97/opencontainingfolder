#include "CmdlineArgv.h"
#include <objbase.h>

#define MY_SKIP_TO_FIRST_NON_WHITESPACE(p) \
	for(; *(p) == ' '; ++(p))


static LPVOID MyAllocZero(SIZE_T cb)
{
	LPVOID ptr = HeapAlloc(GetProcessHeap(), 0, cb);
	if (ptr) { ZeroMemory(ptr, cb); }
	return ptr;
}
static void MyFree(LPVOID ptr)
{
	HeapFree(GetProcessHeap(), 0, ptr);
}
static LPTSTR MyCopyAlloc(LPCTSTR szSrc)
{
	LPTSTR psz = NULL;
	TCHAR const *p = szSrc, *q = NULL;
	SIZE_T cb = 0;
	for (q = p; *q; ++q);
	cb = sizeof(TCHAR) * (q - p);
	psz = MyAllocZero(cb + sizeof(TCHAR));
	if (psz) { CopyMemory(psz, p, cb); }
	return psz;
}
static LPTSTR MyUnquoteCopyAlloc(LPCTSTR szSrc)
{
	LPTSTR psz = NULL;
	TCHAR const *p = szSrc, *q = NULL;
	SIZE_T cb = 0;
	if (p[0] == '"') { ++p; }
	for (q = p; *q; ++q);
	if (q[-1] == '"') { --q; }
	cb = sizeof(TCHAR) * (q - p);
	psz = MyAllocZero(cb + sizeof(TCHAR));
	if (psz) { CopyMemory(psz, p, cb); }
	return psz;
}

static UINT MyCount(LPCTSTR szCmdLine)
{
	UINT count = 0;
	TCHAR const *p = NULL;
	BOOL dq = FALSE; /* met opening dquote? */
	BOOL bs = FALSE; /* met backslash? */
	BOOL sp = FALSE; /* met space outside dquote pair? */
	if (szCmdLine[0] != '\0') { count = 1; }
	for (p = szCmdLine; *p; ++p)
	{
		if (*p == ' ') {
			if (!dq) { 
				/* curr char is space (outside dquote pair) */
				sp = TRUE;
			}
			continue;
		}
		if (sp) {
			sp = FALSE;
			++count;
		}
		if (*p == '\\') {
			bs = !bs;
			continue;
		}
		if (*p == '"') {
			if (!bs) {
				dq = !dq;
			}
		}
		bs = FALSE;
	}
	return count;
}

/*
	**pEnd never NULL after exec.
*/
static void MyLocateCurrAndNext(
	TCHAR const *pStart,
	TCHAR const **ppEnd,
	TCHAR const **ppNext)
{
	TCHAR const *p = NULL, *pEnd = NULL, *pNext = NULL;
	BOOL dq = FALSE;
	for (p = pStart; *p; ++p)
	{
		if (*p == ' ') {
			if (!dq) {
				/* space (outside dquote pair) */
				pEnd = p;
				break;
			}
			continue;
		}
		if (*p == '"') {
			if (p > pStart && p[-1] == '\\') {
				/* escaped double quote \" */
				continue;
			}
			if (dq) {
				pEnd = p + 1;
				break;
			}
			dq = !dq;
		}
	}
	if (!pEnd) {
		pEnd = p;
	}
	for (p = pEnd; *p; ++p)
	{
		if (*p != ' ') {
			pNext = p;
			break;
		}
	}
	*ppEnd = pEnd;
	*ppNext = pNext;
}

static void MyProcess(LPTSTR pszCopy)
{
	TCHAR const *pStart = pszCopy, *pEnd = NULL, *pNext = NULL;
	TCHAR *p = NULL;
	while (TRUE)
	{
		MyLocateCurrAndNext(pStart, &pEnd, &pNext);
		for (p = (TCHAR*)pStart; p < pEnd; ++p)
		{
			/* escaped double quote? */
			if (p[0] == '\\' && p[1] == '"') {
				TCHAR *q = p;
				for (; q < pEnd; q[0] = q[1], ++q);
				--pEnd;
			}
		}
		if (!pNext) {
			*(TCHAR*)pEnd = '\0';
			break;
		}
		for (p = (TCHAR*)pEnd; p < pNext; *p++ = '\0');
		pStart = pNext;
	}
}

static BOOL MyToArray(
	LPCTSTR szProcessed, UINT argc, LPTSTR *argv)
{
	TCHAR const *p = szProcessed;
	UINT i = 0;
	for (; i < argc; ++i)
	{
		LPTSTR psz = NULL;
		/* skip to first non-null */
		for (; !*p; ++p);
		psz = MyUnquoteCopyAlloc(p);
		if (!psz) goto fail;
		argv[i] = psz;
		/* skip to first null */
		for (; *p; ++p);
	}
	return TRUE;
fail:
	while (i > 0)
	{
		MyFree(argv[--i]);
	}
	return FALSE;
}

EXTERN_C
void __stdcall
CmdlineArgv_FreeArgv(
	LPTSTR *argv,
	UINT argc)
{
	UINT i = 0;
	for (; i < argc; ++i)
	{
		MyFree(argv[i]);
	}
	MyFree(argv);
}

EXTERN_C
BOOL __stdcall
CmdlineArgv_ParseAlloc(
	LPCTSTR szCmdline,
	UINT *pArgc,
	LPTSTR **pArgv)
{
	BOOL ok = FALSE;
	UINT argc = 0;
	LPTSTR *argv = NULL, pszCopy = NULL;
	if (!szCmdline || !pArgc) goto eof;
	MY_SKIP_TO_FIRST_NON_WHITESPACE(szCmdline);
	argc = MyCount(szCmdline);
	if (argc > 0 && pArgv)
	{
		argv = MyAllocZero(sizeof(LPTSTR) * argc);
		pszCopy = MyCopyAlloc(szCmdline);
		if (!argv || !pszCopy) goto eof;
		MyProcess(pszCopy);
		ok = MyToArray(pszCopy, argc, argv);
		if (!ok) goto eof;
		*pArgv = argv;
	}
	*pArgc = argc;
	ok = TRUE;
eof:
	if (!ok && argv) { MyFree(argv); }
	if (pszCopy) { MyFree(pszCopy); }
	return ok;
}

EXTERN_C
BOOL __stdcall
CmdlineArgv_LocateArgByIndex(
	LPCTSTR szCmdline,
	UINT iArg,
	TCHAR const **ppszArg,
	TCHAR const **ppNullTermi)
{
	TCHAR const *pStart = szCmdline;
	TCHAR const *pEnd = NULL, *pNext = NULL;
	UINT iCurr = 0;
	if (!szCmdline || !ppszArg) {
		return FALSE;
	}
	for (; pStart; pStart = pNext, ++iCurr)
	{
		MyLocateCurrAndNext(pStart, &pEnd, &pNext);
		if (iCurr == iArg) {
			*ppszArg = pStart;
			if (ppNullTermi) { *ppNullTermi = pEnd; }
			return TRUE;
		}
	}
	return FALSE;
}
