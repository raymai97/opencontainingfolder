#ifndef _MYWINAPI_CMDLINEARGV_H
#define _MYWINAPI_CMDLINEARGV_H

#include <Windows.h>

EXTERN_C
void __stdcall
CmdlineArgv_FreeArgv(
	LPTSTR *argv,
	UINT argc
);

/*
	Usage: ...&argc, &argv) where argc is UINT and argv is LPTSTR*.
	If you need argc only, pass NULL for pArgv.
	Whitespace chars at beginning of szCmdline are skipped.
	Don't assume argc always > 0.
*/
EXTERN_C
BOOL __stdcall
CmdlineArgv_ParseAlloc(
	LPCTSTR szCmdline,
	UINT *pArgc,
	LPTSTR **pArgv /*opt, me alloc*/
);

/*
	Example command-line (assume iArg = 1):
	abc.exe  "foo"  bar 123
	         ^    ^
	         |    pNullTermi
	         pszArg
*/
EXTERN_C
BOOL __stdcall
CmdlineArgv_LocateArgByIndex(
	LPCTSTR szCmdline,
	UINT iArg,
	TCHAR const **ppszArg,
	TCHAR const **ppNullTermi /*opt*/
);

#endif/*_MYWINAPI_CMDLINEARGV_H*/
