#ifndef _MYWINAPI_OPENFOLDER_H
#define _MYWINAPI_OPENFOLDER_H
/* use ole32, MyWinAPI_Pidl */
#include <Windows.h>

/*
	Support any Windows since Win95 and NT4.0,
	with or without Active Desktop.

	NOTE:
	OpenFolder...ByPath() functions check path validity.
	Disable WOW64 FS redirection to avoid potential issue.
*/

typedef struct _ITEMIDLIST const UNALIGNED *LPCITEMIDLIST;

EXTERN_C
HRESULT __stdcall
OpenFolderAndSelectItemByPath(
	LPCTSTR szPath
);

EXTERN_C
HRESULT __stdcall
OpenFolderAndSelectItemByPidl(
	LPCITEMIDLIST pidl
);

EXTERN_C
HRESULT __stdcall
OpenFolderByPath(
	LPCTSTR szPath
);

EXTERN_C
HRESULT __stdcall
OpenFolderByPidl(
	LPCITEMIDLIST pidl
);

#endif/*_MYWINAPI_OPENFOLDER_H*/
