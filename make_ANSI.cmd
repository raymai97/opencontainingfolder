@echo off
call D:\vc60\vcvar.cmd || goto :BadEnv
cd %~dp0
set include=%~dp0;%include%

cl -nologo -O2 -MD ^
	-DWIN32_LEAN_AND_MEAN ^
	MyWinAPI/CmdlineArgv.c ^
	MyWinAPI/OpenFolder.c ^
	MyWinAPI/Pidl.c ^
	Program.c ^
	-link ^
	-opt:nowin98 ^
	-out:OpenContainingFolder_ANSI.exe ^
	-entry:RawMain ^
	-subsystem:Windows ^
	kernel32.lib ^
	user32.lib ^
	ole32.lib ^
	advapi32.lib || pause
del /q *.obj
exit/b

:BadEnv
echo MSVC6.0 build env not ready.
pause
exit/b 1
