/* NOT FOR USER - FOR USE BY Pidl.c ONLY  */

#include <objbase.h>

typedef struct MY_IPersistIDList MY_IPersistIDList_t;
typedef struct MY_IPersistFolder2 MY_IPersistFolder2_t;

#define INHERIT_IUNKNOWN_AS(SELF) \
	HRESULT(__stdcall *QueryInterface)( \
		SELF *pSelf, void *piid, void **ppObj); \
	ULONG(__stdcall *AddRef)( \
		SELF *pSelf); \
	ULONG(__stdcall *Release)( \
		SELF *pSelf)

#define INHERIT_IPERSIST_AS(SELF) \
	HRESULT(__stdcall *GetClassID)( \
		SELF *pSelf, CLSID *pClassId)

#define INHERIT_IPERSISTIDLIST_AS(SELF) \
	HRESULT(__stdcall *SetIDList)( \
		SELF *pSelf, LPCITEMIDLIST pidl); \
	HRESULT(__stdcall *GetIDList)( \
		SELF *pSelf, LPITEMIDLIST *ppidl)

#define INHERIT_IPERSISTFOLDER_AS(SELF) \
	HRESULT(__stdcall *Initialize)( \
		SELF *pSelf, LPCITEMIDLIST pidl)

#define INHERIT_IPERSISTFOLDER2_AS(SELF) \
	HRESULT(__stdcall *GetCurFolder)( \
		SELF *pSelf, LPITEMIDLIST *ppidl)

struct MY_IPersistIDList {
	struct {
		INHERIT_IUNKNOWN_AS(MY_IPersistIDList_t);
		INHERIT_IPERSIST_AS(MY_IPersistIDList_t);
		INHERIT_IPERSISTIDLIST_AS(MY_IPersistIDList_t);
	} *lpVtbl;
};

struct MY_IPersistFolder2 {
	struct {
		INHERIT_IUNKNOWN_AS(MY_IPersistFolder2_t);
		INHERIT_IPERSIST_AS(MY_IPersistFolder2_t);
		INHERIT_IPERSISTFOLDER_AS(MY_IPersistFolder2_t);
		INHERIT_IPERSISTFOLDER2_AS(MY_IPersistFolder2_t);
	} *lpVtbl;
};

static IID const
s_IID_IPersistIDList = {
	0x1079acfc, 0x29bd, 0x11d3, { 0x8e, 0x0d, 0x00, 0xc0, 0x4f, 0x68, 0x37, 0xd5 }
},
s_IID_IPersistFolder2 = {
	0x1ac3d9f0, 0x175c, 0x11d1, { 0x95, 0xbe, 0x00, 0x60, 0x97, 0x97, 0xea, 0x4f }
};
