#ifndef _MYWINAPI_PIDLFROMPATH_H
#define _MYWINAPI_PIDLFROMPATH_H
/* use ole32 */
#include <Windows.h>

/*
	A file-system path can represent file-system object (file and dir) only.
	PIDL and IShellItem can represent any Shell object like My Computer.
	IShellItem is more COM-oriented and recent compared to PIDL,
	but PIDL compatibility rocks; it is there since Windows 95.

	ParseName is Shell namespace name. It is more robust than file-system path.
	Every file-system path is a valid ParseName, but not vice-versa.
	For example, "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}" is valid ParseName,
	but not a valid file-system path. Microsoft call it "ParsingName".
*/

typedef struct _ITEMIDLIST UNALIGNED *LPITEMIDLIST;
typedef struct _ITEMIDLIST const UNALIGNED *LPCITEMIDLIST;
typedef struct IUnknown IUnknown;
typedef struct IShellItem IShellItem;

EXTERN_C
void __stdcall
Pidl_FreePidl(
	LPITEMIDLIST pidl
);

EXTERN_C
void __stdcall
Pidl_FreeStr(
	LPTSTR psz
);

EXTERN_C
HRESULT __stdcall
Pidl_CreateFromPath(
	LPCTSTR szPath,
	LPITEMIDLIST *ppidl
);

EXTERN_C
HRESULT __stdcall
Pidl_CreateFromUnk(
	IUnknown *pUnk,
	LPITEMIDLIST *ppidl
);

EXTERN_C
HRESULT __stdcall
Pidl_GetDispNameAlloc(
	LPCITEMIDLIST pidl,
	LPTSTR *ppsz
);

EXTERN_C
HRESULT __stdcall
Pidl_GetParseNameAlloc(
	LPCITEMIDLIST pidl,
	LPTSTR *ppsz
);

EXTERN_C
HRESULT __stdcall
Pidl_GetPathAlloc(
	LPCITEMIDLIST pidl,
	LPTSTR *ppsz
);

EXTERN_C
HRESULT __stdcall
Pidl_ToShellItem(
	LPCITEMIDLIST pidl,
	IShellItem **ppsi
);

EXTERN_C
HRESULT __stdcall
Pidl_Clone(
	LPCITEMIDLIST pidl,
	LPITEMIDLIST *ppidl
);

EXTERN_C
UINT __stdcall
Pidl_GetSize(
	LPCITEMIDLIST pidl
);

EXTERN_C
LPCITEMIDLIST __stdcall
Pidl_LastId(
	LPCITEMIDLIST pidl
);
EXTERN_C
LPITEMIDLIST __stdcall
Pidl_LastIdMut(
	LPITEMIDLIST pidl
);

EXTERN_C
BOOL __stdcall
Pidl_IsEqual(
	LPCITEMIDLIST pidl1,
	LPCITEMIDLIST pidl2
);

#endif/*_MYWINAPI_PIDLFROMPATH_H*/
